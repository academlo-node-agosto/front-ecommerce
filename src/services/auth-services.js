// import {config} from 'dotenv';
import Service from './index';

// config();

const model = "users";

// const base = process.env.REACT_APP_SERVICE_BASE_URL;
const version = process.env.REACT_APP_SERVICE_VERSION;

const authService = new Service(version, model);

export default authService;


