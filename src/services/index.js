class Service {
    constructor(version, model){
        this.version = version;
        this.model = model;
    }

    async get(extra=''){
        try{
            let response = await fetch(`${this.version}/${this.model}${extra}`);
            let results = await response.json();            
            if(response.status >= 400){
                return this.response(false, true, response.status, results.message, results.message);
            }else{
                return this.response(true, false, response.status, results, null);
            }
        }catch(error){
            return this.response(false, true, 500, null, error.message);
        }
    }

    async post(body, extra=''){
        try{
            let response = await fetch(`${this.version}/${this.model}${extra}`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            });
            let results = await response.json();
            if(response.status >= 400){
                return this.response(false, true, response.status, results.message, results.message);
            }else{
                return this.response(true, false, response.status, results, results.message);
            }
        }catch(error){
            return this.response(false, true, 500, null, error.message);
        }
    }

    response(success, error, status, results, message){
        return {
            success,
            error,
            status,
            results,
            message
        }
    }
}

export default Service;