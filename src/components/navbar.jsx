import React from 'react';
import {makeStyles} from '@material-ui/core';
import {Link} from "react-router-dom";
import {connect} from 'react-redux';
import {useHistory} from "react-router-dom";

const useStyles = makeStyles(() => ({
    navContainer: {
        width: "100%",
        padding: "0em 5vw",
        display: "flex",
        alignItems: "center",
        justifyContent: "flex-end",
        "& ul": {
            margin: 0
        },
        "& ul>li": {
            display: "inline-block",
            padding: "2vh 0em",
            "& a": {
                color: "#000",
                textTransform: "uppercase",
                textDecoration: "none",
                padding: "0em 2em",
                fontSize: "16px",
                letterSpacing: "1px"
            }
        }
    },
    logoutBtn: {
        border: "none",
        backgroundColor: "#3f51b5",
        color: "#FFF",
        padding: "10px 15px",
        borderRadius: "4px",
        textTransform: "uppercase",
        cursor: "pointer",
        transition: ".5s",
        "&:hover": {
            transition: ".5s",
            backgroundColor: "#6578dc",
            color: "#fff"
        }
    }
}));

export default function Navbar() {
    const classes = useStyles();
    let history = useHistory();

    const logout = async() => {
        const response = await fetch(`/v1/users/logout`, {method: "POST"});
        console.log(response.ok);
        if (response.ok) {
            history.push("/", {authenticated: true});
        } else {
            console.log(response);
        }
    }

    return (
        <div className={classes.navContainer}>
            <nav>
                <ul>
                    <li>
                        <Link to="/">Inicio</Link>
                    </li>
                    <li>
                        <Link to="/registro">Registro</Link>
                    </li>
                    <li>
                        <Link to="/acerca">Acerca de</Link>
                    </li>
                    {

                    <li>
                        <button className={classes.logoutBtn} onClick={logout}>Cerrar Sesión</button>
                    </li>
                    }
                </ul>
            </nav>
        </div>
    )
}

// const mapStateToProps = (state) => {
//     return {
//         user: state.userAuth
//     }
// }

// export default connect(mapStateToProps, null)(Navbar)