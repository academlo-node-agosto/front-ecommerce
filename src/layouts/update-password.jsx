import React, {useState, useEffect} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import AuthService from '../services/auth-services';
import {useHistory, useLocation} from 'react-router-dom';
import Alert from '@material-ui/lab/Alert';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://academlo.com/">
                Ecommerce
            </Link>{' '} {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));

export default function SignUp() {
    const classes = useStyles();
    const [formPassword,
        setFormPassword] = useState({password: '', passwordr: ''});
    const [validation,
        setValidation] = useState(false);
    const [resetPasswordRes, setResetPasswordRes] = useState({success: false, error: false, message: ''})

    useEffect(() => {
        checkPasswords(formPassword);
    }, [formPassword]);
    
    const location = useLocation();
    const history = useHistory();

    const handleInput = (event) => {
        setFormPassword({
            ...formPassword,
            [event.target.name]: event.target.value
        });
    }

    const updatePassword = async (form, event) => {
        event.preventDefault();
        //Query params
        const search = location.search;
        const params = new URLSearchParams(search);

        let userId = params.get("user");
        let token = params.get("token");
        console.log(search);

        if (form.password.length >= 6 && form.passwordr.length >= 6) {
            let response = await AuthService.post({...formPassword, id: userId, token: token},"/update-password");
            if (response.success) {
                setResetPasswordRes({...resetPasswordRes, success: true, error: false, message: response.message})
                setTimeout(() => {
                    history.push("/", {authenticated: false});
                }, 4000);
            } else {
                setResetPasswordRes({...resetPasswordRes, success: false, error: true, message: response.message})
                console.log(response);
            }
        } else {
            return false;
        }
    }

    const checkPasswords = (fPassword) => {
        if ((fPassword.password === fPassword.passwordr) || (fPassword.password.lenght === 0 && fPassword.passwordr.lenght === 0)) {
            setValidation(false);
        } else {
            setValidation(true);
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Recuperar contraseña
                </Typography>
                <form className={classes.form} noValidate onInput={handleInput} onSubmit={(event) => updatePassword(formPassword, event)}>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                error={validation}
                                variant="outlined"
                                required
                                fullWidth
                                id="password"
                                label="Contraseña"
                                name="password"
                                type="password"
                                min
                                inputProps={{
                                minLength: "6"
                            }}
                                autoComplete="password"/>
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={validation}
                                variant="outlined"
                                required
                                fullWidth
                                id="passwordr"
                                label="Repetir Contraseña"
                                name="passwordr"
                                type="password"
                                min
                                inputProps={{
                                minLength: "6"
                            }}
                                helperText={validation
                                ? "Las contraseñas no coinciden"
                                : ""}/>
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}>
                        Enviar
                    </Button>
                    {
                        resetPasswordRes.success && (
                            <Box mt={2}>
                                <Alert  severity="success">{resetPasswordRes.message}</Alert>
                            </Box>
                        )
                    }
                    {
                        resetPasswordRes.error && (
                            <Box mt={2}>
                                <Alert  severity="error">{resetPasswordRes.message}</Alert>
                            </Box>
                        )
                    }
                </form>
            </div>
            <Box mt={5}>
                <Copyright/>
            </Box>
        </Container>
    );
}