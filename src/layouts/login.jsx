import React, {useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import loginImg from '../img/login.jpg';
import AuthService from '../services/auth-services';
import Alert from '@material-ui/lab/Alert';
import {
  useHistory
} from "react-router-dom";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://academlo.com/">
        Ecommerce
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '95vh',
  },
  image: {
    backgroundImage: `url(${loginImg})`,
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export default function SignInSide() {
  const [loginForm, setLoginForm] = useState({email: '', password: ''});
  const [loginValidation, setLoginValidation] = useState({error: false, warning: false, message: ''});
  let history = useHistory();
  const classes = useStyles();

  const login = async (event) => {
    event.preventDefault();
    if(loginForm.email.length > 0 && loginForm.password.length > 0){
      const response = await AuthService.post(
        loginForm,
        '/login'
      );
      if(response.success){
        history.push("/dashboard", {authenticated: true});
      }else{
        console.log(response);
        setLoginValidation({...loginValidation, warning: false, error: response.error, message: response.message});      
      }
    }else{
      setLoginValidation({...loginValidation, warning: true, error: false, message: 'Hay campos vacios'});
    }
  }

  const onChangeInput = (event) => {
    setLoginForm({...loginForm, [event.target.name] : event.target.value });
  }

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Inicio de Sesión
          </Typography>
          <form className={classes.form} noValidate onSubmit={login} onInput={onChangeInput}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Correo Electrónico"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Contraseña"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Recordarme"
            />
            {
              loginValidation.warning && (
                <Box mt={2}>
                  <Alert  severity="warning">{loginValidation.message}</Alert>
                </Box>
              )
            }
            {
              loginValidation.error && (
                <Box mt={2}>
                  <Alert  severity="error">{loginValidation.message}</Alert>
                </Box>
              )
            }
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Iniciar Sesión
            </Button>
            
            <Grid container>
              <Grid item xs>
                <Link to="/recuperar-contrasena" variant="body2">
                  He olvidado mi contraseña
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"¿Aún no tienes una cuenta? Registrate"}
                </Link>
              </Grid>
            </Grid>
           
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}
