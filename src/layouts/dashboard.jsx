import React from 'react';
import {Redirect, useLocation} from "react-router-dom";
const Dashboard = (props) => {
    let location = useLocation();
    let state = location.state;
    let authenticated = props.authenticated ? props.authenticated : (state ? state.authenticated : false); 
    if (authenticated) {
        return (
            <div>
                Dashboard
            </div>
        )
    } else {
        return <Redirect
            to={{
            pathname: "/",
            state: {
                from: ''
            }
        }}/>
    }
}

export default Dashboard;