import React, {useState, useEffect} from "react";
import {
  Switch,
  Route,
  useHistory
} from "react-router-dom";
import Login from './layouts/login';
import Register from './layouts/register';
import LostPassword from './layouts/lost-password';
import UpdatePassword from './layouts/update-password';
import Navbar from './components/navbar';
import Dashboard from './layouts/dashboard';
import AuthService from './services/auth-services';


export default function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(false);
  let history = useHistory();

  useEffect(() => {
    const authenticated = async () => {
      let response = await AuthService.get('/me');
      if(response.success){
        setIsAuthenticated(true);
        //Redireccionar hacía dashboard
        history.push("/dashboard");
      }else{
        setIsAuthenticated(false);
      }
    }
    authenticated();
  }, []);

  return (
      <div>
        <Navbar />
        <Switch>
          <Route path="/acerca">
            {/* <About /> */}
          </Route>
          <Route path="/dashboard">
            <Dashboard authenticated={isAuthenticated} />
          </Route>
          <Route path="/actualizar-contrasena">
            <UpdatePassword />
          </Route>
          <Route path="/recuperar-contrasena">
            <LostPassword />
          </Route>
          <Route path="/registro">
            <Register />
          </Route>
          <Route path="/">
            <Login />
          </Route>
        </Switch>
      </div>
  );
}